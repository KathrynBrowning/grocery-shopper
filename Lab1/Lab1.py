# Program Name:         Lab1.py
# Course:               IT1113/Section W01
# Student Name:         Kathryn Browning
# Assignment Number:    Lab1
# Due Date:             01/28/2018

# This program asks for the name and price of 8 items purchased
# by a customer at a grocery store, then displays that information.
def main():
    print('Welcome customer! Please enter the name and price of each of your items.\n')

    first_item_name = input('What is the name of the first item?\n')
    first_item_price = input('What is the price of that item?\n')
    second_item_name = input('\nWhat is the name of the second item?\n')
    second_item_price = input('What is the price of that item?\n')
    third_item_name = input('\nWhat is the name of the third item?\n')
    third_item_price = input('What is the price of that item?\n')
    fourth_item_name = input('\nWhat is the name of the fourth item?\n')
    fourth_item_price = input('What is the price of that item?\n')
    fifth_item_name = input('\nWhat is the name of the fifth item?\n')
    fifth_item_price = input('What is the price of that item?\n')
    sixth_item_name = input('\nWhat is the name of the sixth item?\n')
    sixth_item_price = input('What is the price of that item?\n')
    seventh_item_name = input('\nWhat is the name of the seventh item?\n')
    seventh_item_price = input('What is the price of that item?\n')
    eighth_item_name = input('\nWhat is the name of the eighth item?\n')
    eighth_item_price = input('What is the price of that item?\n')

    print('\nYour products are displayed below:')
    display_products(first_item_name, first_item_price)
    display_products(second_item_name, second_item_price)
    display_products(third_item_name, third_item_price)
    display_products(fourth_item_name, fourth_item_price)
    display_products(fifth_item_name, fifth_item_price)
    display_products(sixth_item_name, sixth_item_price)
    display_products(seventh_item_name, seventh_item_price)
    display_products(eighth_item_name, eighth_item_price)

    print('\nThank you for shopping with us today!')

# Display the names and prices
def display_products(name, price):
    print('{:<30} {:<5}'.format(name, price))

# Call main function
main()